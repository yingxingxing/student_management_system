const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave: false,
  // vue-cli 设置代理服务器
  devServer: {
    // pablicPath: '/',
    port: 8081, // 默认端口为8080，此端口冲突,也会出现代理异常的情况
    host: 'localhost', // 本地的Ipv4地址
    open: true, // 启动打开浏览器
    hot: true, // 热更新
    historyApiFallback: true,
    compress: true,

    proxy: {
      '/api': {
        target: 'http://localhost:8080/', // 后端接口的根目录
        ws: false,
        changeOrigin: true, // 是否跨域
        pathRewrite: {
          '^/api': '/api'
        }
      },
      '/dev-api': {
        target: 'http://localhost:8081/', // 服务器代理地址,这里的地址会代替axios设置的baseURL地址
        changeOrigin: true,
        ws: false,
        pathRewrite: {
          '^/dev-api': ''
        }
      }
    }

  }
})
