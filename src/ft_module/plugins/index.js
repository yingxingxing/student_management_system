// 引入我们封装好的js
import information from '../information'
export default {
  // 这里应该算是传递一个Vue的实例过来
  install (Vue) {
    // 模态框对象
    Vue.prototype.$modal = information
  }
}
