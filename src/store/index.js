import Vue from 'vue'
import Vuex from 'vuex'
import { getToken } from '@/utils/storage'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: getToken()
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
