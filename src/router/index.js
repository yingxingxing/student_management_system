import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login',
    hidden: false,
    component: () => import('@/views/Login')
  },
  {
    path: '/login',
    name: 'Login',
    hidden: false,
    component: () => import('@/views/Login')
  },
  {
    path: '*',
    name: 'NotFound',
    hidden: false,
    component: () => import('@/views/NotFound')
  },

  {
    path: '/homeView',
    name: '首页',
    hidden: true,
    iconClass: 'el-icon-location',
    redirect: '/homeView/index',
    component: () => import('@/views/HomeView'),
    children: [

      {
        path: '/homeView/index',
        name: '首页',
        iconClass: 'el-icon-location',
        component: () => import('@/views/home/index')
      }

    ]
  },
  {
    path: '/homeView',
    name: '班级学生管理',
    hidden: true,
    iconClass: 'el-icon-location',
    redirect: '/homeView/infoList',
    component: () => import('@/views/HomeView'),
    children: [

      {
        path: '/homeView/infoList',
        name: '学生列表',
        iconClass: 'el-icon-location',
        component: () => import('@/views/students/infoList')
      },
      {
        path: '/homeView/infoListse',
        name: '信息管理',
        iconClass: 'el-icon-location',
        component: () => import('@/views/students/infoListse')
      },
      {
        path: '/homeView/StudentList',
        name: '作业列表',
        iconClass: 'el-icon-location',
        component: () => import('@/views/students/StudentList')
      },
      {
        path: '/homeView/WorkList',
        name: '学生列表',
        iconClass: 'el-icon-location',
        component: () => import('@/views/students/WorkList')
      },
      {
        path: '/homeView/WorkMent',
        name: '学生列表',
        iconClass: 'el-icon-location',
        component: () => import('@/views/students/WorkMent')
      }
    ]
  },
  {
    path: '/homeView',
    name: '系统信息管理',
    hidden: true,
    iconClass: 'el-icon-location',
    redirect: '/homeView/department',
    component: () => import('@/views/HomeView'),
    children: [

      {
        path: '/homeView/department',
        name: '部门管理',
        iconClass: 'el-icon-location',
        component: () => import('@/views/system/department')
      },
      {
        path: '/homeView/staffList',
        name: '员工管理',
        iconClass: 'el-icon-location',
        component: () => import('@/views/system/staffList')
      }
      // {
      //   path: '/homeView/StudentList',
      //   name: '作业列表',
      //   iconClass: 'el-icon-location',
      //   component: () => import('@/views/system/StudentList')
      // },
      // {
      //   path: '/homeView/WorkList',
      //   name: '学生列表',
      //   iconClass: 'el-icon-location',
      //   component: () => import('@/views/system/WorkList')
      // },
      // {
      //   path: '/homeView/WorkMent',
      //   name: '学生列表',
      //   iconClass: 'el-icon-location',
      //   component: () => import('@/views/system/WorkMent')
      // }
    ]
  },
  {
    path: '/homeView',
    name: '数据统计管理',
    iconClass: 'el-icon-location',
    hidden: true,
    redirect: '/homeView/dataAnalysis',
    component: () => import('@/views/HomeView'),
    children: [
      {
        path: '/homeView/dataAnalysis',
        name: '数据概览',
        iconClass: 'el-icon-location',
        component: () => import('@/views/dataAnalysis/DataView')
      },
      {
        path: '/homeView/MapView',
        name: '地图概览',
        iconClass: 'el-icon-location',
        component: () => import('@/views/dataAnalysis/MapView')
      },
      {
        path: '/homeView/ScoreMap',
        name: '分数地图',
        iconClass: 'el-icon-location',
        component: () => import('@/views/dataAnalysis/ScoreMap')
      },
      {
        path: '/homeView/TravelMap',
        name: '旅游地图',
        iconClass: 'el-icon-location',
        component: () => import('@/views/dataAnalysis/TravelMap')
      }

    ]
  },
  {
    path: '/homeView',
    name: '用户中心',
    hidden: true,
    iconClass: 'el-icon-location',
    redirect: '/homeView/User',
    component: () => import('@/views/HomeView'),
    children: [
      {
        path: '/homeView/User',
        name: '用户中心',
        iconClass: 'el-icon-location',
        component: () => import('@/views/users/User')
      }

    ]
  }
]
// 防止连续点击多次路由报错
const routerPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location) {
  return routerPush.call(this, location).catch(err => err)
}

// eslint-disable-next-line no-use-before-define


const router = new VueRouter({
  routes,
  mode: 'history', // hash模式具备#号,
  scrollBehavior: () => ({ y: 0 })
})
// 注册一个全局前置守卫
// router.beforeEach(async (to, from, next) => {
// /**
//  * to:即将要进入的目标用一种标准化的方式
//  * from: 当前导航正要离开的路由用一种标准化的方式
//  * */
//   // 如果用户访问的登录页,直接放行
//   if (to.name === 'Login') return next()
//   // 从sessionStorage中获取到保存的token值
// })
router.onError((error) => {
  const pattern = /Loading chunk (\d)+ failed/g
  const isChunkLoadFailed = error.message.match(pattern)
  if (isChunkLoadFailed) {
    // 用路由的replace方法，并没有相当于F5刷新页面，失败的js文件并没有从新请求，会导致一直尝试replace页面导致死循环，而用 location.reload 方法，相当于触发F5刷新页面，虽然用户体验上来说会有刷新加载察觉，但不会导致页面卡死及死循环，从而曲线救国解决该问题
    location.reload()
    // const targetPath = $router.history.pending.fullPath;
    // $router.replace(targetPath);
  }
})
export default router
