import axios from 'axios'
import { getToken } from '@/utils/storage'

axios.defaults.baseURL = '/api'
const service = axios.create({
  // baseURL: 'http://localhost:8081/api', // baseURL会自动加在请求地址上
  timeout: 3000
})
// 添加请求拦截器
service.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么? 在请求之前做些什么(获取并设置token)
  config.headers.token = getToken() // 设置请求头
  return config
}, function (error) {
  // 对请求错误做些什么?
  return Promise.reject(error)
})

// 添加响应拦截器
service.interceptors.response.use(function (response) {
  // 2xx 范围内的状态码都会触发该函数。
  // 范围内的状态码都会触发该函数
  // 对响应数据做点什么
  console.log(response)
  const {
    code,
    msg
  } = response.data
  if (code !== 1 && code !== 200) {
    return Promise.reject(msg)
  }
  return response.data
}, function (error) {
  // 超出 2xx 范围的状态码都会触发该函数。
  // 对响应错误做点什么
  return Promise.reject(error)
})
export default service
