import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './assets/index.less'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import plugins from './ft_module/plugins/index'
// 打开loading 的加载框
// this.$modal.loading('加载中')
// this.$modal.closeLoading()
Vue.config.productionTip = false
Vue.use(plugins)
Vue.use(ElementUI)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
