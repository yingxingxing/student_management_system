import request from '@/utils/server'

/**
 * 查询部门管理列表
 * */
export function listMyTodo () {
  return request({
    url: '/depts',
    method: 'get'
  })
}
/**
 * 根据id输出管理列表
 * */
export function deletelistMyTodo (id) {
  return request({
    url: `/depts/${id}`,
    method: 'delete'
  })
}
/**
 * 新增部门管理列表
 * */
export function insert (data) {
  return request({
    url: '/depts',
    method: 'post',
    data
  })
}
