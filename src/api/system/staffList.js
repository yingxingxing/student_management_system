import request from '@/utils/server'
/**
 * 查询员工列表
 * */
export function liststaff (params) {
  return request({
    url: '/emps',
    method: 'get',
    params
  })
}
